---
sidebar_position: 1
---

# Быстрый старт

Let's discover **Docusaurus in less than 5 minutes**.

## Клонирование проекта

Выполните следующие команды для клонирования сайта

```bash
cd my-website
npm run start
```


